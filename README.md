# Anomaly Detections

The following is the description of a test aimed to replicate a very simplified version of one component ot CorrosionRADAR (CR) analytics: **anomaly detections**.

The goal of this test is to implement this simplified version of anomaly detections using the following brief.

As for every real-world task, the description might not be 100 percent accurate, or explicit. Moreover, not all aspects of the test are described with the same level of details. Any request for clarification is welcome. 

The task must be completed using python. However, while at CR we use FastAPI & Docker, any equivalent technology can be used. The test will be evaluated considering: 

  - Correctness 
  - Pythonicity 
  - Code structure 
  - Expressiveness 
  - Behavior (especially on edge cases)


## Data Description

This section describes a simplified version of the data CR collects from their sensors. Hereafter we assume that the sensors we will be working on are moisture sensors. 

The USP of CR is that your proprietary sensors are not spot sensors, but array of sensors. Hence, instead of returning a single sample (representing the moisture in the sensor location), they return a list of samples (representing the moisture throughout the whole sensor length).

### Messages File

The following is a simplified version of a message of one of CR sensors:

```json
    { 
        "id": "cr123", 
        "readings": [ 
            103.04983414952939, 
            13.06129559147759, 
            11.423143599058566, 
            15.192366557718543, 
            15.072889387425104, 
            42.76854392198215, 
            83.85545790646066, 
            32.14083956177737, 
            10.187591211868611, 
            1.3436639679184346, 
            7.632806716877543 
        ], 
        "step ": 1.0, 
        "timestamp": "2020-01-01T03:00:43" 
    } 
```

The following is the field meaning: 

  - `id`: a unique string to identify the sensor 
  - `readings`: a list containing the moisture values measured by the sensor throughout its length.
  - `step`: distance between two consecutive readings
  - `timestamp`: acquisition timestamp of the message.

The file [messages.json](messages.json) contains a list of messages. 

### Sensor Model
The following is a simplified version of a sensor model, which is obtained averaging several messages:

```json
{ 
    "id": "cr123", 
    "step ": 1.0, 
    "means": [ 
        104.32191308650778, 
        13.67750843709852, 
        10.344738896946733, 
        13.195271382650805, 
        22.53947465254497, 
        37.340696571314815, 
        77.7254312595879, 
        28.40130246469284, 
        10.08883150873186, 
        3.46104589054122, 
        3.2674249590475952 
    ], 
    "sigma": 3.01, 
    "n_sigma": 3.0 
} 
```

The following is the field meaning:
  - `id`: a unique string to identify the sensor 
  - `step`: distance between 2 consecutive samples 
  - `means`: a list containing the average moisture for every sample. Its length is identical to the length of the samples used to generate the model 
  - `sigma`: standard deviation of the sample means. Valid across the whole means list. 
  - `n_sigma`: number of sigmas beyond which a sample is considered an anomaly

The file [model.json](model.json) contains a single sensor model. 


## The Task

The test is composed by three logic components:

```mermaid
graph LR
    BL(Business Logic) --> WA(Web Application) --> C(Container)
```

In the **business logic** we need to check if a message contains anomalies. Then, the business logic has to be exposed as a REST APIs in a **web application**. Finally,the whole application should run inside a **container**.


### Business logic

The $i^{th}$ single reading of a message is considered an anomaly if the following expression is true:

```math
|readings_{i} - means_{i}| > n_{\sigma} \cdot \sigma
```

To count the number of anomalies of a given message, we need to apply a sensor model to the sensor reading and count for how many samples the above expression is true.

Message and model must have the same `id`, otherwise the process is meaningless.


### Web Application

The business logic has to be exposed as a REST APIs in a web application. The application has to accept a reading as argument.

Normally, the application will try to fetch a valid sensor model from a database based on the `id` of the incoming message. However, in this simplified case we can assume the application has already in memory all the available models.

### Container

The whole application has to run in a container.

## Question

How many anomalies are contained in each message?