from typing import Tuple, Optional, Dict, Union, List
import asyncio
import json

VALUES = Union[str, float, List[float]]

__default_model: Dict[str, VALUES]
__given_messages: List[Dict[str, VALUES]]
with open('model.json') as model:
    __default_model = json.load(model)
with open('messages.json') as messages:
    __given_messages = json.load(messages)

ID       = "id"
STEP     = "step "
MEANS    = "means"
READINGS = "readings"
N_SIGMA  = "n_sigma"
SIGMA    = "sigma"

__models = [__default_model]
_models_map:Dict[str, VALUES] = {}
for m in __models:
    _models_map[m[ID]] = m

SLEEP_INTERVAL = 1.0

class Mock_DB(object):

    async def get_model(self, id_: str) -> Optional[Dict[str, VALUES]]:
        await asyncio.sleep(SLEEP_INTERVAL)
        return _models_map.get(id_, None)


    async def update_model(self, id_: str, m: Dict[str, VALUES]) -> None:
        assert id_ == m[ID]
        await asyncio.sleep(SLEEP_INTERVAL)
        _models_map[id_] = m


def check_msg(msg: Dict[str, VALUES], model: Dict[str, VALUES]) -> Tuple[Optional[List[int]], Union[int, str]]:
    # validate inputs
    if msg[ID] != model[ID]:
        return None, "Mismatch between message id {} and model id {}".format(msg[ID], model[ID])
    if msg[STEP] != model[STEP]:
        return None, "Mismatch between message step {:f} and model step {:f}".format(msg[STEP], model[STEP])
    if len(msg[READINGS]) != len(model[MEANS]):
        return None, "Mismatch between message readings len {:d} and model means len {:d}".format(len(msg[READINGS]),
                                                                                                  len(model[MEANS]))
    # detect anomaly
    anomalies = []
    for i, r in enumerate(msg[READINGS]):
        if abs(r - model[MEANS][i]) > (model[N_SIGMA] * model[SIGMA]):
            anomalies.append(i)
    return anomalies, len(anomalies)


def t():
    # [([], 0), ([], 0), ([], 0), ([5, 6], 2), ([6], 1), ([8], 1), ([7], 1), ([6], 1), ([], 0), ([1, 2], 2),
    # (None, 'Mismatch between message id cr121 and model id cr123'),
    # (None, 'Mismatch between message step 1.250000 and model step 1.000000'),
    # (None, 'Mismatch between message id cr121 and model id cr123')]
    res = []
    for m in __given_messages:
        res.append(check_msg(m, __default_model))
    return res
