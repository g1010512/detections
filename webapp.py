from typing import Optional, Dict
import asyncio
import json
import tornado.web
import argparse

import messageChecker


PORT = 8888
DB = messageChecker.Mock_DB()

class WellcomeHandler(tornado.web.RequestHandler):

    def get(self) -> None:
        self.write("Welcome, POST to /checkMsg to use the message checker API")

class CheckMessageHandler(tornado.web.RequestHandler):

    async def post(self) -> None:
        message = json.loads(self.request.body.decode('utf-8'))
        id_ = message[messageChecker.ID]
        model: Optional[Dict[str, messageChecker.VALUES]] = await DB.get_model(id_)
        if model is None:
            response = "Error: no model with id {}".format(id_)
        else:
            res, msg = messageChecker.check_msg(message, model)
            if res is None:
                response = msg
            else:
                response = "{} {} found in message {}".format(msg, "anomalies" if msg > 0 else "anomaly", message)
        self.write(repr(response))


def make_app():
    return tornado.web.Application([
        (r"/", WellcomeHandler),
        (r"/checkMsg", CheckMessageHandler),
    ])


async def main():
    app = make_app()
    app.listen(PORT)
    await asyncio.Event().wait()


def run():
    asyncio.run(main())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, help='webapp api port')
    args = parser.parse_args()
    if getattr(args, "port", None) is not None:
        PORT = args.port
    run()
